// получение всех объектов
const sizePizza = document.getElementsByName("size"), // выбор размера кнопками
    table = document.querySelector(".table img"), // изображение пицы
    ingridients = document.querySelectorAll(".ingridients img"), // выбор ингридиентов
    price = document.querySelectorAll(".price"), // цена
    passForSize = /px/;

//переменные
let tableHeight = window.getComputedStyle(table).height,
    tableWidth = window.getComputedStyle(table).width,
    tableHeightNum = tableHeight.split(passForSize),
    tableWidthNum = tableWidth.split(passForSize),

    cost = 0,
    cost_pizzaSize = 0,
    cost_pizzaIngridients = 0,
    cost_pizzaSauces = 0,
    cost_pizzaTopings = 0;

// ------------------------------------------------------------------------
// изменение размера пицы
for (let i = 0; i < sizePizza.length; i++) {
    sizePizza[i].addEventListener('click', () => {
        if (sizePizza[i].checked) {
            switch (sizePizza[i].id) {
                case 'small':
                    table.style.height = 0.8 * tableHeightNum[0] + 'px';
                    table.style.width = 0.8 * tableWidthNum[0] + 'px';
                    cost += 50;
                    price.innerHTML = cost;
                    break;
                case 'mid':
                    table.style.height = 0.9 * tableHeightNum[0] + 'px';
                    table.style.width = 0.9 * tableWidthNum[0] + 'px';
                    cost += 100;
                    price.innerHTML = cost;
                    break;
                case 'big':
                    table.style.height = tableHeightNum[0] + 'px';
                    table.style.width = tableWidthNum[0] + 'px';
                    cost += 150;
                    price.innerHTML = cost;
                    break;
                default:
                    break;
            }
        }
    })
}
//-------------------------------------------------------------------------


/* Drag-and-drop
ingridients.onMouseDown = function(event) {
    let shiftX = event.clientX - ingridients.getBoundingClientRect().left;
    let shiftY = event.clientY - ingridients.getBoundingClientRect().top;
    ingridients.style.position = 'absolute';
    ingridients.style.zIndex = 1000;
    document.body.append(ingridients);
    moveAt(event.pageX, event.pageY);

    function moveAt(pageX, pageY) {
        ingridients.style.left = pageX - shiftX + 'px';
        ingridients.style.top = pageY - shiftY + 'px';
    }

    function onMouseMove(event) {
        moveAt(event.pageX, event.pageY);
        ingridients.hidden = true;
        let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
        ingridients.hidden = false;
        if (!elemBelow) return;
        let droppableBelow = elemBelow.closest('table');
        if (currentDroppable != droppableBelow) {
            if (currentDroppable) {
                leaveDroppable(currentDroppable);
            }
            currentDroppable = droppableBelow;
            if (currentDroppable) {
                enterDroppable(currentDroppable);
            }
        }
    }

    document.addEventListener('mousemove', onMouseMove);

    ingridients.onmouseup = function() {
        document.removeEventListener('mousemove', onMouseMove);
        ingridients.onmouseup = null;
    };
}

ingridients.ondragstart = function() {
    return false;
};
-------------------------*/

//подсчет стоимости
cost = cost_pizzaSize + cost_pizzaIngridients + cost_pizzaSauces + cost_pizzaTopings;